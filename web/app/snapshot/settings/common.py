# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from os.path import dirname, abspath
from socket import getfqdn, gethostname

# Application path
BASE_DIR = dirname(dirname(dirname(abspath(__file__))))

# Pool connection
POOL_CONN_MIN = 5
POOL_CONN_MAX = 10

# Redirect or serve files
REDIRECT_TO_FARM = False

# Cache timeout

CACHE_TIMEOUT_DEFAULT = 600

CACHE_TIMEOUT_MR_INDEX = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_MR_PACKAGE = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_MR_VERSION = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_MR_TIMESTAMP = CACHE_TIMEOUT_DEFAULT

CACHE_TIMEOUT_REMOVAL = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_REMOVAL_ONE = CACHE_TIMEOUT_DEFAULT

CACHE_TIMEOUT_PACKAGE_INDEX = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_PACKAGE_PACKAGE = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_PACKAGE_VERSION = CACHE_TIMEOUT_DEFAULT

CACHE_TIMEOUT_ARCHIVE_INDEX = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_ARCHIVE_DIR = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_ARCHIVE_REDIRECT = CACHE_TIMEOUT_DEFAULT

CACHE_TIMEOUT_ROOT = CACHE_TIMEOUT_DEFAULT
CACHE_TIMEOUT_ROOT_NEWS = CACHE_TIMEOUT_DEFAULT

# Mail settings
# Defaults settings works with a local MTA
MAIL_DEFAULT_SENDER = f'snapshot@{getfqdn()}'

# Hostname lookup
HOSTNAME = gethostname()
