# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2021 Baptiste Beauplat <lyknode@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import abort


def test_error_no_mail(bad_app, mailbox):
    bad_app.config['MAIL_ADMINS'] = None
    client = bad_app.test_client()

    response = client.get('/mr/package/')

    assert response.status_code == 500
    assert len(mailbox) == 0


def test_error_send_failed(bad_app, mailbox):
    bad_app.config['MAIL_ADMINS'] = ['sna\npshot@localhost']
    client = bad_app.test_client()

    response = client.get('/mr/package/')

    assert response.status_code == 500
    assert len(mailbox) == 0


def test_error_notify_admins(bad_app, mailbox):
    client = bad_app.test_client()

    response = client.get('/mr/package/')

    assert response.status_code == 500
    assert len(mailbox) == 1
    assert mailbox[0].subject == 'Exception in Snapshot web app'
    assert 'self.conn = pool.getconn()' in mailbox[0].body


def test_error_notify_no_exception(bad_app, mailbox):
    @bad_app.route('/abort')
    def raise_500():
        abort(500)

    client = bad_app.test_client()

    response = client.get('/abort')

    assert response.status_code == 500
    assert len(mailbox) == 1
    assert mailbox[0].subject == 'Exception in Snapshot web app'
    assert 'No original exception' in mailbox[0].body
