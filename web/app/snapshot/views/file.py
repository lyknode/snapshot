# snapshot.debian.org - web frontend
#
# Copyright (c) 2021 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger
from flask import Blueprint, abort, send_from_directory, make_response

from snapshot.controllers.archive import ArchiveController, ArchiveError, \
    ArchiveDeniedError
from snapshot.lib.helpers import set_download_name

log = getLogger(__name__)
router = Blueprint("file", __name__, url_prefix="/file")


@router.route("/<digest>")
@router.route("/<digest>/<filename>")
def file_index(digest, filename=""):
    try:
        node = ArchiveController.get_node_by_hash(digest)
    except ArchiveDeniedError as e:
        abort(403, str(e))
    except ArchiveError as e:
        abort(404, str(e))

    send_args = set_download_name(node.get_orig_name())
    send_file = send_from_directory(node.get_dir(), node.get_filename(),
                                    **send_args)

    # Remove content type to mimic current snapshot behavior
    response = make_response(send_file)
    response.headers.pop('Content-Type')

    return response
