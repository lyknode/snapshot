# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from pytest import raises


def test_root_index(client, dataset):
    response = client.get('/')

    # The root page works
    assert response.status_code == 200
    assert 'The snapshot archive is a wayback machine' in response.data.decode()

    # The news section works
    assert '<h2>2019-09-29</h2>' in response.data.decode()

    # The sidebar works
    assert 'Browse ftp archive snapshots from one of the following archives' \
        in response.data.decode()

    # Archive list
    for archive in dataset.get_archives():
        assert f'<a href="archive/{archive}/">{archive}</a>' in \
            response.data.decode()

    # Source and binary packages dirs list
    for package in dataset.get_packages_dirs():
        assert f'<a href="package/?cat={package}">{package}</a>' in \
            response.data.decode()
        assert f'<a href="binary/?cat={package}">{package}</a>' in \
            response.data.decode()


def test_root_index_without_database(app, client):
    app.pool = None

    with raises(AttributeError):
        assert client.get('/')


def test_root_old_news(client, breadcrumbs):
    response = client.get('/oldnews')
    crumbs = [
        {
            'url': '/',
            'name': 'localhost',
        },
        {
            'name': 'older news',
            'sep': '',
        },
    ]

    # The news page works
    assert response.status_code == 200
    assert '<h2>2010-02-20</h2>' in response.data.decode()

    # And the breadcrums
    assert breadcrumbs.exists(crumbs, response.data.decode())
