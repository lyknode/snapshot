# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010, 2015 Peter Palfrader
# Copyright (c) 2021 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger

from re import sub
from os.path import join
from urllib.parse import quote

from flask import request, abort, Blueprint, redirect, render_template, \
    current_app

from snapshot.lib.cache import cache
from snapshot.lib.control_helpers import get_domain, \
    link_quote_array, build_url_archive
from snapshot.models.snapshot import get_snapshot_model

log = getLogger(__name__)
pkg_router = Blueprint('package', __name__, url_prefix='/package')
bin_router = Blueprint('binary', __name__, url_prefix='/binary')


def _build_crumbs(pkg=None, version=None, start=None, is_binary=False):
    crumbs = []

    url = quote(request.environ.get('SCRIPT_NAME')) + '/'
    crumbs.append({'url': url, 'name': get_domain(), 'sep': '|'})

    if is_binary:
        crumbs.append({'url': None, 'name': 'binary package:', 'sep': ''})
    else:
        crumbs.append({'url': None, 'name': 'source package:', 'sep': ''})

    if not start:
        if pkg.startswith('lib') and len(pkg) >= 4:
            start = pkg[0:4]
        else:
            start = pkg[0:1]

    if is_binary:
        url += 'binary/'
    else:
        url += 'package/'

    crumbs.append({
        'url': f'{url}?cat={quote(start)}',
        'name': f'{start}*',
    })

    if pkg is not None:
        url += f'{quote(pkg)}/'
        crumbs.append({'url': url, 'name': pkg})

        if version:
            url += f'{quote(version)}/'
            crumbs.append({'url': url, 'name': version})

    crumbs[-1]['url'] = None

    return crumbs


def _attribute_escape(attribute):
    return sub('[^a-zA-Z0-9_.-]', lambda m: ':%x:' % (ord(m.group())),
               attribute)


def _ensure_ascii(*kvargs):
    # Package names are ascii.
    # Check that before passing it on to postgres since the DB
    # will just whine about not being able to convert the string
    # anyway.
    # If the passed string is not ascii, then the package name
    # simply does not exist.
    for string in kvargs:
        try:
            string.encode('ascii')
        except UnicodeEncodeError:
            abort(404, 'No such package')


@pkg_router.route('/')
@cache()
def package_root():
    package_root.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_PACKAGE_INDEX'
    ]
    source = request.args.get('src', None)
    start = request.args.get('cat', None)

    if source:
        return redirect(quote(f'{source}/'))

    if start:
        pkgs = get_snapshot_model().packages_get_name_starts_with(start)

        if not pkgs:
            abort(404, 'No source packages in this category.')

        packages = link_quote_array(pkgs)
        breadcrumbs = _build_crumbs(start=start)
        title = f'{start}*'

        return render_template('package/package-list-packages.html',
                               title=title, start=start, packages=packages,
                               breadcrumbs=breadcrumbs)

    return redirect('../')


@pkg_router.route('/<source>/')
@cache()
def package_source(source):
    package_source.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_PACKAGE_PACKAGE'
    ]

    _ensure_ascii(source)

    source_versions = get_snapshot_model().packages_get_source_versions(source)

    if not source_versions:
        abort(404, 'No such source package')

    source_versions = link_quote_array(source_versions)
    breadcrumbs = _build_crumbs(source)

    return render_template('package/package-source.html', src=source,
                           title=source, sourceversions=source_versions,
                           breadcrumbs=breadcrumbs)


@pkg_router.route('/<source>/<version>/')
@cache()
def package_source_version(source, version):
    package_source_version.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_PACKAGE_VERSION'
    ]

    _ensure_ascii(source, version)

    source_files = get_snapshot_model() \
        .packages_get_source_files(source, version)
    binpkgs = get_snapshot_model() \
        .packages_get_binpkgs_from_source(source, version)

    if not source_files or not binpkgs:
        abort(404, 'No source or binary packages found')

    binpkgs = [dict(pkg) for pkg in binpkgs]
    binhashes = []
    bin_id_list = (binpkg['binpkg_id'] for binpkg in binpkgs)
    bin_files = get_snapshot_model().packages_get_binary_files_from_ids(
        bin_id_list)

    for binpkg in binpkgs:
        binpkg['escaped_name'] = _attribute_escape(binpkg['name'])
        binpkg['escaped_version'] = _attribute_escape(binpkg['version'])
        binpkg['files'] = [
            binfile['hash'] for binfile in bin_files[binpkg['binpkg_id']]
        ]
        binhashes += binpkg['files']

    fileinfo = get_snapshot_model().packages_get_file_info(
        source_files + binhashes)

    for digest in fileinfo:
        for info in fileinfo[digest]:
            info['dirlink'] = build_url_archive(info['archive_name'],
                                                info['run'],
                                                info['path'])
            info['link'] = build_url_archive(info['archive_name'],
                                             info['run'],
                                             join(info['path'], info['name']),
                                             isadir=False)

    source_files.sort(key=lambda index: (fileinfo[index][0]['name'], index)
                      if len(fileinfo[index]) > 0
                      else ('', index))  # reproducible file order

    breadcrumbs = _build_crumbs(source, version)
    title = f'{source} ({version})'

    return render_template('package/package-source-one.html', src=source,
                           version=version, sourcefiles=source_files,
                           binpkgs=binpkgs, fileinfo=fileinfo,
                           breadcrumbs=breadcrumbs, title=title)


@bin_router.route('/')
@cache()
def package_binary_root():
    package_binary_root.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_PACKAGE_INDEX'
    ]
    binary = request.args.get('bin', None)
    start = request.args.get('cat', None)

    if binary:
        return redirect(quote(f'{binary}/'))

    if start:
        pkgs = get_snapshot_model() \
            .packages_get_name_starts_with(start, get_binary=True)

        if not pkgs:
            abort(404, 'No binary packages in this category.')

        packages = link_quote_array(pkgs)
        breadcrumbs = _build_crumbs(start=start, is_binary=True)
        title = f'{start}*'

        return render_template('package/package-binary-list-packages.html',
                               start=start, packages=packages,
                               breadcrumbs=breadcrumbs, title=title)

    return redirect('../')


@bin_router.route('/<binary>/')
def package_binary(binary):
    package_binary.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_PACKAGE_PACKAGE'
    ]
    _ensure_ascii(binary)

    binary_versions = get_snapshot_model() \
        .packages_get_binary_versions_by_name(binary)

    if not binary_versions:
        abort(404, 'No such binary package')

    binary_versions = [dict(version) for version in binary_versions]

    for version in binary_versions:
        version['link'] = quote(f'../../package/{version["source"]}/'
                                f'{version["version"]}/')
        version['escaped_name'] = _attribute_escape(version['name'])
        version['escaped_binary_version'] = _attribute_escape(
            version['binary_version']
        )

    breadcrumbs = _build_crumbs(binary, is_binary=True)

    return render_template('package/package-binary.html', binary=binary,
                           title=binary, binaryversions=binary_versions,
                           breadcrumbs=breadcrumbs)
