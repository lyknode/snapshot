# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger

log = getLogger(__name__)


class DatasetController():
    def __init__(self, dataset):
        self.dataset = dataset

    def get_archives(self):
        archives = set()

        for items in self.dataset.values():
            for item in items:
                archives.add(item)

        return sorted(archives)

    def get_packages_dirs(self):
        packages = self.get_packages()
        packages_dirs = set()

        for package in packages:
            if package.startswith('lib'):
                packages_dirs.add(package[:4])
            else:
                packages_dirs.add(package[:1])

        return sorted(packages_dirs)

    def get_packages(self):
        packages = set()

        for runs in self.dataset.values():
            for archives in runs.values():
                for distribs in archives.values():
                    for package in distribs.keys():
                        packages.add(package)

        return sorted(packages)

    def get_versions(self, package):
        versions = set()

        for runs in self.dataset.values():
            for archives in runs.values():
                for distribs in archives.values():
                    for package_name, version in distribs.items():
                        if package_name == package:
                            versions.add(version)

        return sorted(versions)

    def get_runs(self):
        runs = {}

        for run, archives in self.dataset.items():
            for archive in archives.keys():
                runs.setdefault(archive, []).append(run)

        return runs
