# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger
from flask import Blueprint, render_template, request, abort, redirect, \
    url_for, send_from_directory, make_response, current_app

from snapshot.lib.cache import cache
from snapshot.models.snapshot import get_snapshot_model
from snapshot.controllers.archive import ArchiveController, ArchiveError, \
    ArchiveDir, ArchiveRedirect
from snapshot.lib.control_helpers import build_url_archive, get_domain
from snapshot.lib.helpers import set_download_name

log = getLogger(__name__)
router = Blueprint("archive", __name__, url_prefix="/archive")


@router.route("/")
@cache()
def archive_index():
    archive_index.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_ARCHIVE_INDEX'
    ]

    return redirect(url_for('root.index'))


@router.route("/<archive>/")
@cache()
def archive_archive(archive):
    archive_archive.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_ARCHIVE_INDEX'
    ]
    year = request.args.get('year', None)
    month = request.args.get('month', None)

    if year is not None and month is not None:
        try:
            runs = ArchiveController.get_archive_runs(archive, year, month)
        except ArchiveError as e:
            abort(404, str(e))

        breadcrumbs = _build_crumbs(archive, year=int(year), month=int(month))
        month = f'{int(month):02}'
        title = f'{archive}:{year}-{month}'

        return render_template('archive/archive-runs.html', title=title,
                               archive=archive, year=year, month=month,
                               breadcrumbs=breadcrumbs, runs=runs)

    yearmonths = get_snapshot_model() \
        .mirrorruns_get_yearmonths_from_archive(archive)
    breadcrumbs = _build_crumbs(archive)

    if not yearmonths:
        abort(404, f'Archive "{archive}" does not exist')

    return render_template('archive/archive.html', title=archive,
                           archive=archive, yearmonths=yearmonths,
                           breadcrumbs=breadcrumbs)


@router.route("/<archive>/<date>/")
@router.route("/<archive>/<date>/<path:path>")
@cache()
def archive_dir(archive, date, path='/'):
    archive_dir.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_ARCHIVE_INDEX'
    ]

    try:
        node = ArchiveController.get_node(archive, date, path)
    except ArchiveError as e:
        abort(404, str(e))
    except ArchiveRedirect as e:
        archive_dir.cache_timeout = current_app.config[
            'CACHE_TIMEOUT_ARCHIVE_REDIRECT'
        ]
        return redirect(str(e))

    if isinstance(node, ArchiveDir):
        archive_dir.cache_timeout = current_app.config[
            'CACHE_TIMEOUT_ARCHIVE_DIR'
        ]
        breadcrumbs = _build_crumbs(archive, node.info["run"],
                                    node.info["stat"]["path"])
        title = f'{archive}:{node.info["stat"]["path"]} ' \
                f'{node.info["run"]["run"]}'

        return render_template('archive/archive-dir.html', title=title,
                               breadcrumbs=breadcrumbs,
                               **node.info)

    # node is an ArchiveFile
    send_args = set_download_name(node.get_orig_name())
    send_file = send_from_directory(node.get_dir(), node.get_filename(),
                                    **send_args)

    # Remove content type to mimic current snapshot behavior
    response = make_response(send_file)
    response.headers.pop('Content-Type')

    return response


def _build_crumbs(archive, run=None, path=None, year=None, month=None):
    crumbs = []

    url = build_url_archive()
    crumbs.append({'url': url, 'name': get_domain(), 'sep': '|'})

    crumbs.append({'url': None, 'name': 'archive:', 'sep': ''})

    url = build_url_archive(archive)
    crumbs.append({'url': url, 'name': archive, 'sep': ''})

    if run:
        year = run['run'].year
        month = run['run'].month

    if year is not None or month is not None:
        crumbs.append({'url': f"{url}?year={year}&month={month}",
                       'name': f'({year}-{int(month):02})'})

    if run:
        crumbs.append({'url': build_url_archive(archive, run['run']),
                       'name': run['run']})

        if path and path != '/':
            full_path = ''

            for path_element in path.strip('/').split('/'):
                full_path += f'{path_element}/'
                crumbs.append({'url': build_url_archive(archive, run['run'],
                                                        full_path),
                               'name': path_element})

    crumbs[-1]['url'] = None

    return crumbs
