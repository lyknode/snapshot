#!/usr/bin/python3
#
# Copyright (c) 2024 Philipp Kern
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

def upgrade(db):
    db.execute("""
        CREATE INDEX IF NOT EXISTS binpkg_name
            ON binpkg USING btree (name);
        CREATE INDEX IF NOT EXISTS binpkg_srcpkg_id
            ON binpkg USING btree (srcpkg_id);
        """)
    db.execute("""
        CREATE MATERIALIZED VIEW IF NOT EXISTS package_names AS
            SELECT DISTINCT 'srcpkg' AS type, name FROM srcpkg
            UNION ALL
                SELECT DISTINCT 'binpkg' AS type, name FROM binpkg;
        CREATE INDEX IF NOT EXISTS package_names_type_name
            ON package_names USING btree (type, name);
        CREATE MATERIALIZED VIEW IF NOT EXISTS package_prefixes AS
            SELECT DISTINCT
                type,
                CASE WHEN name LIKE 'lib%' THEN SUBSTRING(name FROM 1 FOR 4)
                     ELSE SUBSTRING(name FROM 1 FOR 1)
                END AS start
            FROM package_names
            ORDER BY type, start ASC;
        """)

    db.execute("UPDATE config SET value='25' WHERE name='db_revision' AND value='24'")

# vim:set et:
# vim:set ts=4:
# vim:set shiftwidth=4:
