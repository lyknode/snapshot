# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010, 2015 Peter Palfrader
# Copyright (c) 2021, 2023 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
from re import match

from flask import Blueprint, jsonify, abort, request, current_app

from snapshot.lib.cache import cache
from snapshot.lib.control_helpers import rfc3339_timestamp
from snapshot.views.package import _ensure_ascii
from snapshot.models.snapshot import get_snapshot_model

log = logging.getLogger(__name__)
router = Blueprint("mr", __name__, url_prefix="/mr")


def _get_fileinfo_for_mr_one(digest):
    fileinfo = get_snapshot_model().packages_get_file_info([digest])[digest]

    for info in fileinfo:
        info['first_seen'] = rfc3339_timestamp(info['run'])
        del info['run']

    return fileinfo


def _get_fileinfo_for_mr(hashes):
    result = {}

    for digest in hashes:
        result[digest] = _get_fileinfo_for_mr_one(digest)

    return result


@router.route("/timestamp/")
@cache()
def mr_timestamp():
    mr_timestamp.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_TIMESTAMP'
    ]
    after = request.args.get('after')
    before = request.args.get('before')
    archive = request.args.get('archive')
    timestamps = {}

    runs = get_snapshot_model().mirrorruns_get_mirrorrun(archive, after,
                                                         before)

    for timestamp, archive in runs:
        timestamps.setdefault(archive, []).append(rfc3339_timestamp(timestamp))

    return jsonify({
        '_comment': "foo",
        'result': timestamps,
    })


@router.route("/package/")
@cache()
def mr_index():
    mr_index.cache_timeout = current_app.config['CACHE_TIMEOUT_MR_INDEX']
    pkgs = get_snapshot_model().packages_get_all()

    return jsonify({
        '_comment': "foo",
        'result': [{'package': pkg} for pkg in pkgs]
    })


@router.route("/package/<source>/")
@cache()
def mr_source(source):
    mr_source.cache_timeout = current_app.config['CACHE_TIMEOUT_MR_PACKAGE']

    _ensure_ascii(source)

    sourceversions = get_snapshot_model() \
        .packages_get_source_versions(source)

    if not sourceversions:
        abort(404, 'No such source package')

    return jsonify({
        '_comment': "foo",
        'package': source,
        'result': [{'version': version} for version in sourceversions]
    })


@router.route("/package/<source>/<version>/srcfiles")
@cache()
def mr_source_version_srcfiles(source, version):
    mr_source_version_srcfiles.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_VERSION'
    ]

    _ensure_ascii(source, version)

    fileinfo = request.args.get('fileinfo', None)
    sourcefiles = get_snapshot_model() \
        .packages_get_source_files(source, version)

    if not sourcefiles:
        abort(404, 'No such source package or no sources found')

    result = {
        '_comment': "foo",
        'package': source,
        'version': version,
        'result': [{'hash': sourcefile} for sourcefile in sourcefiles]
    }

    if fileinfo == '1':
        result['fileinfo'] = _get_fileinfo_for_mr(sourcefiles)

    return jsonify(result)


@router.route("/package/<source>/<version>/binpackages")
@cache()
def mr_source_version_binpackages(source, version):
    mr_source_version_binpackages.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_VERSION'
    ]

    _ensure_ascii(source, version)

    binpkgs = get_snapshot_model() \
        .packages_get_binpkgs_from_source(source, version)

    if not binpkgs:
        abort(404, 'No such source package or no binary packages found')

    binpkgs = [{
        'name': pkg['name'],
        'version': pkg['version']
    } for pkg in binpkgs]

    return jsonify({
        '_comment': "foo",
        'package': source,
        'version': version,
        'result': binpkgs
    })


@router.route("/package/<source>/<version>/"
              "/binfiles/<binary>/<binary_version>")
@cache()
def mr_source_version_binfiles(source, version, binary, binary_version):
    mr_source_version_binfiles.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_VERSION'
    ]

    _ensure_ascii(source, version, binary, binary_version)

    fileinfo = request.args.get('fileinfo', None)
    binfiles = get_snapshot_model() \
        .packages_get_binary_files_from_packagenames(source,
                                                     version,
                                                     binary,
                                                     binary_version)

    if not binfiles:
        abort(404, 'No such package or no binary files found')

    binfiles = [dict(binfile) for binfile in binfiles]
    result = {
        '_comment': "foo",
        'package': source,
        'version': version,
        'binary': binary,
        'binary_version': binary_version,
        'result': binfiles
    }

    if fileinfo == '1':
        result['fileinfo'] = _get_fileinfo_for_mr([binfile['hash'] for binfile
                                                   in binfiles])

    return jsonify(result)


@router.route("/package/<source>/<version>/allfiles")
@cache()
def mr_source_version_allfiles(source, version):
    mr_source_version_allfiles.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_VERSION'
    ]

    _ensure_ascii(source, version)

    fileinfo = request.args.get('fileinfo', None)
    sourcefiles = get_snapshot_model() \
        .packages_get_source_files(source, version)
    binpkgs = get_snapshot_model() \
        .packages_get_binpkgs_from_source(source, version)

    if not sourcefiles or not binpkgs:
        abort(404, 'No source or binary packages found')

    binpkgs = [dict(pkg) for pkg in binpkgs]
    binhashes = []
    bin_id_list = (binpkg['binpkg_id'] for binpkg in binpkgs)
    bin_files = get_snapshot_model().packages_get_binary_files_from_ids(
        bin_id_list)

    for binpkg in binpkgs:
        binpkg['files'] = bin_files[binpkg['binpkg_id']]
        del binpkg['binpkg_id']
        binhashes += [pkg['hash'] for pkg in binpkg['files']]

    result = {
        '_comment': "foo",
        'package': source,
        'version': version,
        'result': {
            'source': [{'hash': sourcefile} for sourcefile in sourcefiles],
            'binaries': binpkgs
        }
    }

    if fileinfo == '1':
        result['fileinfo'] = _get_fileinfo_for_mr(sourcefiles + binhashes)

    return result


@router.route("/binary/<binary>/")
@cache()
def mr_binary(binary):
    mr_binary.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_PACKAGE'
    ]

    _ensure_ascii(binary)

    binaryversions = get_snapshot_model() \
        .packages_get_binary_versions_by_name(binary)

    if not binaryversions:
        abort(404, 'No such binary package')

    binaryversions = [dict(version) for version in binaryversions]
    result = {
        '_comment': "foo",
        'binary': binary,
        'result': binaryversions
    }

    return result


@router.route("/binary/<binary>/<binary_version>/binfiles")
@cache()
def mr_binary_version_binfiles(binary, binary_version):
    mr_binary_version_binfiles.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_VERSION'
    ]

    _ensure_ascii(binary, binary_version)

    fileinfo = request.args.get('fileinfo', None)
    binfiles = get_snapshot_model() \
        .packages_get_binary_files(binary, binary_version)

    if not binfiles:
        abort(404, 'No such package or no binary files found')

    binfiles = [dict(binfile) for binfile in binfiles]
    result = {
        '_comment': "foo",
        'binary': binary,
        'binary_version': binary_version,
        'result': binfiles
    }

    if fileinfo == '1':
        result['fileinfo'] = _get_fileinfo_for_mr([binfile['hash'] for binfile
                                                   in binfiles])

    return result


@router.route("/file/<digest>/info")
@cache()
def mr_fileinfo(digest):
    mr_fileinfo.cache_timeout = current_app.config[
        'CACHE_TIMEOUT_MR_INDEX'
    ]

    # match matches only at start of string
    if not match('[0-9a-f]{40}$', digest):
        abort(404, 'Invalid hash format.')

    return {
        '_comment': "foo",
        'hash': digest,
        'result': _get_fileinfo_for_mr_one(digest)
    }
