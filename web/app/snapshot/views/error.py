# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger
from traceback import format_exception

from flask import render_template, request

from snapshot.lib.email import notify_admin

log = getLogger(__name__)


def error(exception):
    if exception.code >= 500:
        if hasattr(exception, 'original_exception') and \
                exception.original_exception:
            trace = ''.join(format_exception(
                type(exception.original_exception),
                exception.original_exception,
                exception.original_exception.__traceback__
            ))
        else:
            trace = 'No original exception'

        try:
            notify_admin(
                'Exception in Snapshot web app',
                render_template(
                    'mail/error.txt',
                    exception=exception,
                    trace=trace,
                    url=request.url
                )
            )
        except Exception as e:
            log.error('Failed to send exception notification to admins: '
                      f'{str(e)}')

            pass

    return (render_template('base/error.html',
                            exception=exception), exception.code,)
