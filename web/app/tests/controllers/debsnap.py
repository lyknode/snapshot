# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from glob import glob
from os import environ
from os.path import basename
from re import match
from subprocess import check_output
from shutil import rmtree


class DebsnapController():
    def __init__(self, url, workdir):
        self.workdir = workdir
        self._setup_debsnap(url)

    def _setup_debsnap(self, url):
        with open(self.workdir.join('.devscripts'), 'w') as conf:
            conf.write(f'DEBSNAP_BASE_URL={url}\n')

    def _run(self, args):
        env = environ.copy()
        env['HOME'] = self.workdir

        return check_output([
                                'debsnap',
                                '-d',
                                'dest',
                            ] + args,
                            cwd=self.workdir,
                            text=True,
                            timeout=5,
                            env=env)

    def fetch(self, package, version=None):
        self._run([arg for arg in (package, version,) if arg])

        versions = []

        for path in glob(str(self.workdir.join('dest', '*.dsc'))):
            versions.append(match(r'.*_(.*).dsc', basename(path)).group(1))

        rmtree(str(self.workdir.join('dest')))

        return sorted(versions)
