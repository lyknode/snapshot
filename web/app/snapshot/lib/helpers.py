# snapshot.debian.org - web frontend
#
# Copyright (c) 2010, 2011 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from re import sub

from markupsafe import Markup
from flask import __version__ as flask_version


def debian_bugs_markup(text):
    safe = str(Markup.escape(text))
    html = sub(r'#([0-9]+)', r'<a href="https://bugs.debian.org/\1">#\1</a>',
               safe)
    return Markup(html)


def set_download_name(name, params=None):
    if not params:
        params = {}
    if not name:
        return params
    params['as_attachment'] = True
    if flask_version < '2.2.0':
        params['attachment_filename'] = name
    else:
        params['download_name'] = name
    return params
