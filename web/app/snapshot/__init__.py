# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import datetime
from os import makedirs
from os.path import abspath, dirname, join

from flask import Flask
from flask_mail import Mail
from psycopg2.pool import ThreadedConnectionPool
from werkzeug.exceptions import HTTPException

from snapshot.views import root, removal, error, archive, file, package, mr, \
    project
from snapshot.lib.helpers import debian_bugs_markup


def create_app(test_config=None):
    app = Flask(__name__)
    conf_snapshot = join(abspath(dirname(__file__)), 'settings', 'snapshot.py')
    conf_test = join(abspath(dirname(__file__)), 'settings', 'test.py')

    # Load configuration
    if test_config is None:
        app.config.from_pyfile(conf_snapshot, silent=True)  # pragma: no cover
    else:
        app.config.from_pyfile(conf_test, silent=True)
        app.config.update(test_config)

    # ensure the instance folder exists
    try:
        makedirs(app.instance_path)
    except OSError:
        pass

    # Register mailer
    app.mail = Mail(app)

    # Register views
    app.register_error_handler(HTTPException, error.error)
    app.register_blueprint(root.router)
    app.register_blueprint(removal.router)
    app.register_blueprint(archive.router)
    app.register_blueprint(file.router)
    app.register_blueprint(mr.router)
    app.register_blueprint(package.pkg_router)
    app.register_blueprint(package.bin_router)
    app.register_blueprint(project.router)

    # Create connection spool
    app.pool = ThreadedConnectionPool(
        app.config['POOL_CONN_MIN'],
        app.config['POOL_CONN_MAX'],
        **app.config['DATABASE'],
    )

    # Addtional jinja attributes
    @app.context_processor
    def jinja_attributes():
        return {
            'now': datetime.now(),
            'debian_bugs_markup': debian_bugs_markup,
            'thishost': app.config['HOSTNAME'],
        }

    return app
