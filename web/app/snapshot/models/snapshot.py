# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010, 2015 Peter Palfrader
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os.path
from flask import current_app, g
# import hashlib

from snapshot.lib.dbinstance import DBInstance


def get_snapshot_model():
    if 'snapshot_model' not in g:
        g.snapshot_model = SnapshotModel(current_app.config['FARM_PATH'],
                                         current_app.pool)

    return g.snapshot_model


class SnapshotModel:
    def __init__(self, farmpath, pool):
        self.farmpath = farmpath
        self.pool = pool

    def archives_get_list(self):
        with DBInstance(self.pool) as db:
            rows = db.query("SELECT name FROM archive ORDER BY name")

        return [row['name'] for row in rows]

    def mirrorruns_get_yearmonths_from_archive(self, archive):
        result = None

        with DBInstance(self.pool) as db:
            rows = db.query("""
                    SELECT archive_id
                      FROM archive
                      WHERE archive.name=%(name)s""",
                            {'name': archive})

            if len(rows) != 0:
                archive_id = rows[0]['archive_id']

                rows = db.query("""
                    SELECT extract(year from run)::INTEGER AS year,
                           extract(month from run)::INTEGER AS month
                      FROM mirrorrun
                      WHERE mirrorrun.archive_id=%(archive_id)s
                      GROUP BY year, month
                      ORDER BY year, month""",
                                {'archive_id': archive_id})

                # make an array like thus:
                #  - year: nnnn
                #    months:
                #      - nn
                #      - nn
                #  - year: nnnn
                #    months:
                #      - nn
                #      - nn
                #      - nn
                result = []
                for row in rows:
                    year, month = row['year'], row['month']
                    if len(result) == 0 or result[-1]['year'] != year:
                        result.append({'year': year, 'months': []})
                    result[-1]['months'].append(month)

        return result

    def mirrorruns_get_runs_from_archive_ym(self, archive, year, month):
        result = None

        with DBInstance(self.pool) as db:
            rows = db.query("""
                    SELECT archive_id
                      FROM archive
                      WHERE archive.name=%(name)s""",
                            {'name': archive})

            if len(rows) != 0:
                archive_id = rows[0]['archive_id']

                result = db.query("""
                    SELECT run
                      FROM mirrorrun
                      WHERE mirrorrun.archive_id=%(archive_id)s
                        AND extract(year from run) = %(year)s
                        AND extract(month from run) = %(month)s
                      ORDER BY run""",
                                  {'archive_id': archive_id,
                                   'year': year,
                                   'month': month})

        return result

    def mirrorruns_get_mirrorrun_at(self, archive, datespec):
        result = None

        with DBInstance(self.pool) as db:
            rows = db.query("""
                    SELECT run, mirrorrun_id, mirrorrun.archive_id
                      FROM mirrorrun JOIN archive
                        ON mirrorrun.archive_id = archive.archive_id
                      WHERE archive.name=%(archive)s
                        AND mirrorrun.run <= %(datespec)s
                      ORDER BY run DESC
                      LIMIT 1""",
                            {'archive': archive,
                             'datespec': datespec})

        if len(rows) != 0:
            result = rows[0]

        return result

    def mirrorruns_get_mirrorrun(self, archive, after, before):
        head = """
            SELECT run, name as archive
              FROM mirrorrun JOIN archive
                USING (archive_id)
        """
        tail = 'ORDER BY run'
        filters = {
            'archive': 'archive.name = %(archive)s',
            'after': 'run >= %(after)s',
            'before': 'run <= %(before)s',
        }

        return self._modular_query(head, filters, tail, archive=archive,
                                   after=after, before=before)

    def _modular_query(self, head, filters, tail, **kwargs):
        condition_keyword = 'WHERE'
        condition = ''

        for key, value in list(kwargs.items()):
            if value is None:
                kwargs.pop(key)
                continue

            condition += f'{condition_keyword} {filters[key]}'

            if condition_keyword == 'WHERE':
                condition_keyword = ' AND'

        query = f'{head} {condition} {tail}'

        with DBInstance(self.pool) as db:
            rows = db.query(query, kwargs)

        return rows

    # @beaker_cache(expire=600, cache_response=False, type='memory',
    #               key='archive')
    # def mirrorruns_get_etag(self, archive):
    #     """We use this result as an identifier for the state of the system,
    #        returning this value as a HTTP ETag token for client side caching
    #        purposes."""
    #     # XXX it might be nice to hash the state of the application
    #     # (code, templates) into this as well.
    #     key = hashlib.sha1()
    #     cursor = None
    #     try:
    #         cursor = db.execute("""SELECT mirrorrun_uuid
    #                                FROM mirrorrun
    #                                WHERE archive_id = (
    #                                  SELECT archive_id FROM archive
    #                                    WHERE name=%(archive)s)
    #                                ORDER BY mirrorrun_uuid""",
    #                             {'archive': archive})
    #         while True:
    #             n = cursor.fetchone()
    #             if n is None:
    #                 break
    #             key.update(n['mirrorrun_uuid'])
    #
    #         return key.hexdigest()
    #     finally:
    #         if not cursor is None:
    #             cursor.close()

    # @beaker_cache(expire=600, cache_response=False, type='memory', key=None)
    # def packages_get_etag(self):
    #     """We use this result as an identifier for the state of the system,
    #        returning this value as a HTTP ETag token for client side caching
    #        purposes."""
    #     # XXX it might be nice to hash the state of the application
    #     # (code, templates) into this as well.
    #     key = hashlib.sha1()
    #     cursor = None
    #     try:
    #         cursor = db.execute("""SELECT mirrorrun_uuid
    #                                FROM mirrorrun JOIN indexed_mirrorrun
    #                                  ON mirrorrun.mirrorrun_id =
    #                                    indexed_mirrorrun.mirrorrun_id
    #                                ORDER BY mirrorrun_uuid""")
    #         while True:
    #             n = cursor.fetchone()
    #             if n is None:
    #                 break
    #             key.update(n['mirrorrun_uuid'])
    #
    #         return key.hexdigest()
    #     finally:
    #         if not cursor is None:
    #             cursor.close()

    # def mirrorruns_get_last_mirrorrun(self, archive):
    #     row = db.query_one("""
    #             SELECT max(run)::TIMESTAMP WITH TIME ZONE AS run
    #               FROM mirrorrun
    #               WHERE archive_id=(
    #                 SELECT archive_id FROM archive WHERE name=%(archive)s)
    #               """,
    #                        {'archive': archive})
    #     if row is None:
    #         return None
    #
    #     return row['run']

    def mirrorruns_get_neighbors(self, mirrorrun_id):
        with DBInstance(self.pool) as db:
            result = db.query_one("""
                SELECT this.run,
                     (SELECT run FROM mirrorrun
                      WHERE mirrorrun.archive_id = this.archive_id
                        AND run > this.run
                      ORDER BY run
                      LIMIT 1) AS next,
                     (SELECT run FROM mirrorrun
                      WHERE mirrorrun.archive_id = this.archive_id
                        AND run < this.run
                      ORDER BY run DESC
                      LIMIT 1) AS prev
                FROM mirrorrun AS this
                WHERE mirrorrun_id=%(mirrorrun_id)s
                  """,
                                  {'mirrorrun_id': mirrorrun_id})

        return result

    def mirrorruns_get_neighbors_change(self, archive_id, mirrorrun_run,
                                        directory_id):
        """Retrun the date of the previous and next mirrorrun where anything
           actually changed in this directory"""
        with DBInstance(self.pool) as db:
            result = db.query_one("""
                SELECT prev.prev, next.next FROM

                        (SELECT min(next.next) AS next FROM
                         (SELECT min(first_run) AS next FROM node_with_ts2
                            WHERE parent = %(directory_id)s
                              AND first_run > %(mirrorrun_run)s
                          UNION ALL
                            SELECT min(run) AS next FROM mirrorrun
                              WHERE archive_id=%(archive_id)s
                                AND run >
                                  (SELECT min(last_run) AS next
                                    FROM node_with_ts2
                                    WHERE parent = %(directory_id)s
                                      AND last_run >= %(mirrorrun_run)s)
                         ) AS next) AS next,

                        (SELECT max(prev.prev) AS prev FROM
                         (SELECT max(run) AS prev FROM mirrorrun
                           WHERE archive_id=%(archive_id)s
                             AND run <
                               (SELECT max(first_run) AS prev FROM node_with_ts2
                                 WHERE parent = %(directory_id)s
                                   AND first_run <= %(mirrorrun_run)s)
                          UNION ALL
                            SELECT max(last_run) AS prev FROM node_with_ts2
                              WHERE parent = %(directory_id)s
                                AND last_run < %(mirrorrun_run)s
                         ) AS prev) AS prev

                  """,
                                  {'mirrorrun_run': mirrorrun_run,
                                   'archive_id': archive_id,
                                   'directory_id': directory_id})

        return result

    def _strip_multi_slash(self, text):
        old = text
        while True:
            text = text.replace('//', '/')
            if text == old:
                break
            old = text
        return text

    def mirrorruns_stat(self, mirrorrun_id, path):
        """'stats' a path in a given mirrorrun.  Will return None if the path
           doesn't exist.
           If it does exist it will do (recursive) symlink resolving and return
           a dict with either file or dir information.
           XXX no idea what it does on danling or cyclic symlinks yet"""
        path = path.rstrip('/')
        path = self._strip_multi_slash(path)
        if path == "":
            path = '/'

        with DBInstance(self.pool) as db:
            try:
                stat = db.query_one("""
                    SELECT filetype,
                           path,
                           directory_id,
                           node_id,
                           digest,
                           size
                      FROM stat(%(path)s, %(mirrorrun_id)s)""",
                                    {'mirrorrun_id': mirrorrun_id,
                                     'path': path})
            except UnicodeEncodeError:
                return None

        if stat['filetype'] is None:
            return None

        return stat

    def mirrorruns_get_first_last_from_node(self, node_id):
        with DBInstance(self.pool) as db:
            first_last = db.query_one("""SELECT first_run, last_run
                                           FROM node_with_ts2
                                           WHERE node_id=%(node_id)s""",
                                      {'node_id': node_id})

        return first_last

    def mirrorruns_readdir(self, mirrorrun_id, path):
        with DBInstance(self.pool) as db:
            readdir = db.query("""SELECT filetype,
                                         name,
                                         digest,
                                         size,
                                         target,
                                         first_run,
                                         last_run
                                    FROM readdir(%(path)s, %(mirrorrun_id)s)
                                    JOIN node_with_ts2
                                      ON readdir.node_id = node_with_ts2.node_id
                                    ORDER BY name""",
                               {'mirrorrun_id': mirrorrun_id,
                                'path': path})

        return readdir

    def get_filepath(self, digest):
        prefix1 = digest[0:2]
        prefix2 = digest[2:4]
        return os.path.join(self.farmpath, prefix1, prefix2, digest)

    def _index_query_results(self, rows, index_name):
        results = {}

        for row in rows:
            row = dict(row)
            index = row.pop(index_name)

            if index not in results:
                results[index] = []

            results[index].append(row)

        return results

    def packages_get_source_versions(self, source):
        with DBInstance(self.pool) as db:
            rows = db.query("""SELECT version
                                 FROM srcpkg
                                 WHERE name=%(source)s
                                 ORDER BY version DESC""",
                            {'source': source})

        return [row['version'] for row in rows]

    def packages_get_source_files(self, source, version):
        with DBInstance(self.pool) as db:
            rows = db.query("""SELECT hash
                               FROM file_srcpkg_mapping
                                   JOIN srcpkg
                                   ON srcpkg.srcpkg_id =
                                     file_srcpkg_mapping.srcpkg_id
                               WHERE name=%(source)s AND version=%(version)s""",
                            {'source': source,
                             'version': version})

        return [row['hash'] for row in rows]

    def packages_get_binpkgs_from_source(self, source, version):
        with DBInstance(self.pool) as db:
            rows = db.query("""SELECT name, version, binpkg_id
                               FROM binpkg
                                 WHERE srcpkg_id =
                                   (SELECT srcpkg_id
                                     FROM srcpkg
                                     WHERE name=%(source)s
                                       AND version=%(version)s)
                               ORDER BY name, version""",
                            {'source': source,
                             'version': version})

        return rows

    def packages_get_binary_versions_by_name(self, binary):
        with DBInstance(self.pool) as db:
            rows = db.query("""SELECT binpkg.name AS name,
                                      binpkg.version AS binary_version,
                                      srcpkg.name AS source,
                                      srcpkg.version AS version
                                 FROM binpkg
                                   JOIN srcpkg
                                   ON binpkg.srcpkg_id=srcpkg.srcpkg_id
                                 WHERE binpkg.name=%(binary)s
                                 ORDER BY binpkg.version DESC""",
                            {'binary': binary})

        return rows

    def packages_get_binary_files(self, binary, binary_version):
        with DBInstance(self.pool) as db:
            rows = db.query("""SELECT hash, architecture
                               FROM file_binpkg_mapping
                                   JOIN binpkg
                                   ON binpkg.binpkg_id =
                                     file_binpkg_mapping.binpkg_id
                               WHERE name=%(binary)s
                                 AND version=%(binary_version)s""",
                            {'binary': binary,
                             'binary_version': binary_version})

        return rows

    def packages_get_binary_files_from_ids(self, binpkg_id_list):
        with DBInstance(self.pool) as db:
            rows = db.query("""SELECT binpkg_id, hash, architecture
                                 FROM file_binpkg_mapping
                                 WHERE binpkg_id IN %(binpkg_id)s
                                 ORDER BY architecture""",
                            {'binpkg_id': tuple(binpkg_id_list)})

        return self._index_query_results(rows, 'binpkg_id')

    def packages_get_binary_files_from_packagenames(self, source, version,
                                                    binary, binary_version):
        with DBInstance(self.pool) as db:
            rows = db.query("""
                SELECT file_binpkg_mapping.architecture,
                       file_binpkg_mapping.hash
                  FROM file_binpkg_mapping
                    JOIN binpkg
                    ON file_binpkg_mapping.binpkg_id = binpkg.binpkg_id
                  WHERE binpkg.srcpkg_id =
                    (SELECT srcpkg_id
                      FROM srcpkg
                      WHERE name=%(source)s
                        AND version=%(version)s)
                        AND binpkg.name = %(binary)s
                        AND binpkg.version = %(binary_version)s""",
                            {'source': source,
                             'version': version,
                             'binary': binary,
                             'binary_version': binary_version})

        return rows

    def packages_get_file_info(self, digest_list):
        with DBInstance(self.pool) as db:
            rows = db.query("""
                SELECT file.hash,
                       file.name,
                       file.size,
                       mirrorrun.run,
                       archive.name as archive_name,
                       directory.path
                  FROM file
                    JOIN node ON file.node_id = node.node_id
                    JOIN mirrorrun ON node.first = mirrorrun.mirrorrun_id
                    JOIN archive ON mirrorrun.archive_id = archive.archive_id
                    JOIN directory ON node.parent = directory.directory_id
                  WHERE hash IN %(hash)s
                  ORDER BY run""",
                            {'hash': tuple(digest_list)})

        return self._index_query_results(rows, 'hash')

    def packages_get_name_starts(self, get_binary=False):
        package_type = ('srcpkg', 'binpkg')[get_binary]
        rows = {}

        with DBInstance(self.pool) as db:
            rows = db.query("""
                SELECT DISTINCT start
                  FROM package_prefixes
                  WHERE type = %(type)s
                  ORDER BY start""",
                            {'type': package_type})

        return [item['start'] for item in rows]

    def packages_get_name_starts_with(self, start, get_binary=False):
        if start not in self.packages_get_name_starts(get_binary):
            return None

        package_type = ('srcpkg', 'binpkg')[get_binary]

        with DBInstance(self.pool) as db:
            if start == "l":
                rows = db.query("""
                    SELECT DISTINCT name
                      FROM package_names
                      WHERE type = %(type)s
                        AND name LIKE %(start)s
                        AND NOT (name LIKE 'lib_%%')
                      ORDER BY name""",
                                {'type': package_type,
                                 'start': f'{start}%'})
            else:
                rows = db.query("""
                    SELECT DISTINCT name
                      FROM package_names
                      WHERE type = %(type)s
                        AND name LIKE %(start)s
                      ORDER BY name""",
                                {'type': package_type,
                                 'start': f'{start}%'})

        return [row['name'] for row in rows]

    def packages_get_all(self):
        with DBInstance(self.pool) as db:
            rows = db.query("""SELECT DISTINCT name FROM srcpkg""")

        return [row['name'] for row in rows]

    def removal_get_list(self):
        with DBInstance(self.pool) as db:
            rows = db.query("""
                SELECT removal_log_id,
                       entry_added,
                       reason
                  FROM removal_log
                  ORDER BY entry_added DESC""")

        return rows

    def removal_get_one(self, key):
        with DBInstance(self.pool) as db:
            row = db.query_one("""
                SELECT removal_log_id,
                       entry_added,
                       reason
                  FROM removal_log
                  WHERE removal_log_id=%(removal_log_id)s""",
                               {'removal_log_id': key})

        return row

    def removal_get_affected(self, key):
        with DBInstance(self.pool) as db:
            rows = db.query("""
                SELECT hash
                  FROM removal_affects
                  WHERE removal_log_id=%(removal_log_id)s""",
                            {'removal_log_id': key})

        return [row['hash'] for row in rows]

    def get_last_mirrorrun(self):
        with DBInstance(self.pool) as db:
            row = db.query_one("""
                SELECT max(run)::TIMESTAMP WITH TIME ZONE AS run
                  FROM mirrorrun
                      """)

        return row['run']
