# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import time

import psycopg2.extras
import psycopg2.pool


class DBInstance:
    MAX_ERR = 10
    MAX_RETRY_OPEN = 20

    def __init__(self, pool):
        self.pool = pool
        delay = 0.0
        for _ in range(0, self.MAX_RETRY_OPEN):
            try:
                self.conn = pool.getconn()
                return
            except psycopg2.pool.PoolError:
                delay += 0.1
        self.conn = pool.getconn()

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        self.close()
        return True

    def _reopen(self, delay=0.0):
        # This condition cannot be tested easily, excluding from coverage
        if not self.conn.closed:  # pragma: no cover
            self.pool.putconn(self.conn, close=True)
        time.sleep(delay)
        self.conn = self.pool.getconn()

    def wrap(self, call, *args, **kw):
        delay = 0.0
        for _ in range(0, self.MAX_ERR):
            try:
                return call(*args, **kw)
            except (psycopg2.OperationalError, psycopg2.InterfaceError):
                try:
                    self._reopen(delay)
                except Exception:
                    pass
                delay += 0.1
        return call(*args, **kw)

    def _execute(self, *args, **kw):
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute(*args, **kw)
        return cursor

    def _query(self, *args, **kw):
        cursor = self.execute(*args, **kw)
        rows = cursor.fetchall()
        cursor.close()
        return rows

    def _query_one(self, *args, **kw):
        result = self.query(*args, **kw)
        if len(result) > 1:
            raise 'Got more than one return in query_one'
        if len(result) == 0:
            return None
        return result[0]

    def execute(self, *args, **kw):
        return self.wrap(self._execute, *args, **kw)

    def query(self, *args, **kw):
        return self.wrap(self._query, *args, **kw)

    def query_one(self, *args, **kw):
        return self.wrap(self._query_one, *args, **kw)

    def close(self):
        try:
            self.pool.putconn(self.conn)
        except (psycopg2.OperationalError, psycopg2.InterfaceError,
                psycopg2.pool.PoolError):
            # Excluding from coverage (see above)
            if not self.conn.closed:  # pragma: no cover
                self.pool.putconn(self.conn, close=True)
