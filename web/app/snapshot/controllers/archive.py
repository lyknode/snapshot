# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger
from datetime import datetime
from os import access, R_OK
from os.path import join, dirname, basename, exists
from re import match
from urllib.parse import quote

from flask import request, current_app, abort

from snapshot.lib.control_helpers import rfc3339_timestamp
from snapshot.models.snapshot import get_snapshot_model

log = getLogger(__name__)


class ArchiveError(Exception):
    pass


class ArchiveDeniedError(Exception):
    pass


class ArchiveRedirect(Exception):
    pass


class ArchiveController():
    @staticmethod
    def get_archive_runs(archive, year, month):
        if not match(r'\d{4}$', year):
            raise ArchiveError(f'Year "{year}" is not valid.')

        if not match(r'\d{1,2}$', month):
            raise ArchiveError(f'Month "{month}" is not valid.')

        runs = get_snapshot_model() \
            .mirrorruns_get_runs_from_archive_ym(archive, year, month)

        if runs is None:
            raise ArchiveError(f'Archive "{archive}" does not exist')

        if len(runs) == 0:
            raise ArchiveError(f'Found no mirrorruns for archive {archive}'
                               f' in {year}-{month}.')

        return list(map(lambda r:
                        {'run': r['run'],
                         # make a machine readable version of a timestamp
                         'run_mr': rfc3339_timestamp(r['run'])
                         }, runs))

    @staticmethod
    def _dateok(date):
        regexp = match(r'(\d{4})(\d{2})(\d{2})(?:T(\d{2})(\d{2})(\d{2})Z)?$',
                       date)
        if regexp is not None:
            year = int(regexp.group(1))
            month = int(regexp.group(2))
            day = int(regexp.group(3))
            hour = int(regexp.group(4)) if regexp.group(4) is not None else 0
            minute = int(regexp.group(5)) if regexp.group(5) is not None else 0
            second = int(regexp.group(6)) if regexp.group(6) is not None else 0

            try:
                datetime(year, month, day, hour, minute, second)
                return True
            except ValueError:
                pass

        if date == "now":
            return True
        return False

    @classmethod
    def get_node(cls, archive, date, url):
        if not cls._dateok(date):
            raise ArchiveError(
                'Invalid date string - nothing to be found here.')

        run = get_snapshot_model().mirrorruns_get_mirrorrun_at(archive, date)

        if run is None:
            raise ArchiveError('No mirrorrun found at this date.')

        stat = get_snapshot_model().mirrorruns_stat(run['mirrorrun_id'],
                                                    f'/{url}')

        if stat is None:
            raise ArchiveError('No such file or directory.')

        if stat['filetype'] == 'd':
            return ArchiveDir(archive, run, stat)

        return ArchiveFile(stat['digest'], stat['path'])

    @staticmethod
    def get_node_by_hash(file_hash):
        # match matches only at start of string
        if match('[0-9a-f]{40}$', file_hash):
            return ArchiveFile(file_hash)

        abort(404, 'Invalid hash format.')


class ArchiveDir():
    def __init__(self, archive, run, stat):
        self.info = {}

        self._redirect_on_link(archive, run, stat)
        self.info['nodes'] = self._get_nodes(run, stat)
        self._build_info(archive, run, stat)

    def _redirect_on_link(self, archive, run, stat):
        realpath = join('/archive', archive,
                        rfc3339_timestamp(run['run']),
                        stat['path'].strip('/'), '')

        if realpath != request.environ['PATH_INFO']:
            raise ArchiveRedirect(realpath)

    def _get_nodes(self, run, stat):
        if stat['path'] != '/':
            nodes = [{'filetype': 'd', 'name': '..', 'quoted_name': '..',
                      'first_run': None, 'last_run': None, 'target': None}]
        else:
            nodes = []

        nodes.extend([
            dict(node) for node in get_snapshot_model().
            mirrorruns_readdir(run['mirrorrun_id'], stat['path'])
        ])

        for node in nodes:
            node['quoted_name'] = quote(node['name'])

            if not node['target'] is None:
                node['quoted_target'] = quote(node['target'])

        return nodes

    def _build_info(self, archive, run, stat):
        node_info = get_snapshot_model(). \
            mirrorruns_get_first_last_from_node(stat['node_id'])
        neighbors = get_snapshot_model(). \
            mirrorruns_get_neighbors(run['mirrorrun_id'])
        neighbors_change = get_snapshot_model(). \
            mirrorruns_get_neighbors_change(run['archive_id'], run['run'],
                                            stat['directory_id'])

        self.info['run'] = run
        self.info['stat'] = stat
        self.info['nav'] = {
          'first':       node_info['first_run'],
          'prev_change': neighbors_change['prev'],
          'prev':        neighbors['prev'],
          'next':        neighbors['next'],
          'next_change': neighbors_change['next'],
          'last':        node_info['last_run']
        }

        for key in list(self.info['nav'].keys()):
            if self.info['nav'][key] is not None:
                self.info['nav'][f'{key}_link'] = join(
                    '/archive', archive,
                    rfc3339_timestamp(self.info['nav'][key]),
                    stat['path'].strip('/'), '')


class ArchiveFile():
    def __init__(self, digest, visiblepath=None):
        # just send a redirect.  And let the caching header follow it.
        # (not all apt clients do http redirects)
        if current_app.config['REDIRECT_TO_FARM'] and \
                visiblepath is not None:
            filename = basename(visiblepath)
            raise ArchiveRedirect(f'/file/{digest}/{quote(filename)}')

        model = get_snapshot_model()
        self.realpath = model.get_filepath(digest)
        if not exists(self.realpath):
            raise ArchiveError('Ooops, we do not have a file with '
                               f'digest {digest} even tho we should. '
                               'You might want to report this.')
        if not access(self.realpath, R_OK):
            raise ArchiveDeniedError(
                f'Ooops, cannot read file with digest {digest}. Maybe this '
                'file is not redistributable and this was done on purpose. '
                'If in doubt report this.')
        self.orig_name = None
        try:
            file_infos = model.packages_get_file_info([digest])[digest]
            if file_infos:
                self.orig_name = file_infos[0]['name']
        except KeyError:
            pass

    def get_dir(self):
        return dirname(self.realpath)

    def get_filename(self):
        return basename(self.realpath)

    def get_orig_name(self):
        return self.orig_name or self.get_filename()
