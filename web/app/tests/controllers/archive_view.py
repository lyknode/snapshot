# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger
from re import search
from datetime import datetime

log = getLogger(__name__)


class ArchiveViewController():
    def __init__(self, snapshot):
        self.snapshot = snapshot

    def has_dates(self, archive, haystack):
        dates = self.snapshot.get_dates(archive)
        needle = r'<ul>\s+'

        for date in dates:
            needle += fr'<li>{date["year"]}:\s+'

            for month in date['months']:
                needle += fr'<a href="\./\?year={date["year"]}&amp;'
                needle += fr'month={month}">{int(month):02}</a>\s+'

            needle += r'</li>\s+'

        needle += r'</ul>\s+'

        return search(needle, haystack)

    def has_runs(self, archive, year, month, haystack):
        runs = self.snapshot.get_runs(archive, year, month)
        needle = fr'<h1>Archive {archive}</h1>\s+'
        needle += fr'<h2>{year}-{int(month):02}</h2>\s+<p>\s+'

        for run in runs:
            needle += fr'<a href="{run.strftime("%Y%m%dT%H%M%SZ")}/">'
            needle += fr'{run.isoformat(" ")}</a><br />\s+'

        needle += r'</p>'

        return search(needle, haystack)

    def has_files(self, archive, date, path, haystack):
        files = self.snapshot.get_files(archive, date, path)
        needle = r'<table class="readdir">\s+<tr>\s+<th>&nbsp;</th>\s+'
        needle += r'<th>Name</th>\s+<th style=\'text-align: right\'>Size</th>'
        needle += r'\s+<th>first seen</th>\s+</tr>\s+'

        if path != '/':
            needle += r'<tr>\s+<td>d</td>\s+<td colspan="2"><a href="../">../'
            needle += r'</a></td>\s+<td>\s+</td>\s+</tr>\s+'

        for info in files:
            if info['type'] == 'file':
                needle += r'<tr>\s+<td>-</td>\s+<td>'
                needle += fr'<a href="{info["name"]}">{info["name"]}</a>'
                needle += r'</td>\s+<td style=\'text-align: right\'>'
                needle += fr'{info["size"]}</td>\s+<td>\s+{info["last"]}'

            elif info['type'] == 'symlink':
                needle += r'<tr>\s+<td>l</td>\s+<td colspan="2">\s+'
                needle += fr'<a href="{info["name"]}">{info["name"]}</a>'
                needle += fr' -&gt;\s+<a href="{info["dest"]}">{info["dest"]}'
                needle += fr'</a>\s+</td>\s+<td>\s+{info["last"]}'

            else:
                needle += r'<tr>\s+<td>d</td>\s+<td colspan="2">'
                needle += fr'<a href="{info["name"]}/">{info["name"]}/</a>'
                needle += fr'</td>\s+<td>\s+{info["last"]}'

            needle += r'\s+</td>\s+</tr>\s+'

        needle += r'</table>'

        return search(needle, haystack)

    def has_timeline(self, archive, date, path, haystack):
        (first, last) = self.snapshot.get_first_last(archive, path)
        (prev, following) = self.snapshot.get_prev_next(archive, date)
        (prev_change, following_change) = self.snapshot.get_neighbors_changes(
            archive, date, path
        )
        last_value = None
        needle = r'<div class="timeline">\s+'
        run = datetime.strptime(date, '%Y%m%dT%H%M%SZ')

        for title, date in (
            ('first', first),
            ('prev change', prev_change),
            ('prev', prev),
            (None, run),
            ('next', following),
            ('next change', following_change),
            ('last', last),
                ):
            if not date:
                continue

            if title != 'first':
                if date == last_value and date != run:
                    needle += r'&nbsp;&nbsp; = &nbsp;&nbsp;\s+'
                else:
                    needle += r'&nbsp;&nbsp; \| &nbsp;&nbsp;\s+'

            if title == 'first' and date == run:
                needle += r'No previous version of this directory '
                needle += r'available.\s+'
            elif title == 'last' and date == run:
                needle += r'No later version of this directory '
                needle += r'available.\s+'
            elif not title:
                needle += fr'<strong>{date.isoformat(" ")}</strong>\s+'
            else:
                needle += fr'<acronym title="{date.isoformat(" ")}">'
                needle += fr'<a href="/archive/{archive}/'
                needle += fr'{date.strftime("%Y%m%dT%H%M%SZ")}{path}'
                if not path.endswith('/'):
                    needle += r'/'
                needle += fr'">{title}</a></acronym>\s+'

            last_value = date

        needle += r'</div>\s+'

        return search(needle, haystack)
