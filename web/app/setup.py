#!/usr/bin/python3
import sys
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))
sys.path.insert(0, here)

from project import PROJECT, VERSION, AUTHOR  # noqa: E402


# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    # Project name and version
    name=PROJECT,
    version=VERSION,

    # Descriptions
    description='the software running behind snapshot.debian.org',
    long_description=long_description,

    # Homepage for mentors
    url='https://snapshot.debian.org',

    # Authors
    author=AUTHOR,
    author_email='debian-snapshot@lists.debian.org',

    # Related metadata
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.11',
        'Framework :: Flask :: 1.0',
    ],
    keywords='debian snapshot archive packages',

    # What packages to install
    packages=find_packages(exclude=['docs', 'tests', 'old']),
    data_files=[('.', ['project.py'])],

    # Python version requirements
    python_requires='>=3.11, <4',

    # Requirements
    install_requires=[
        'flask>=2.2.2',
        'flask-mail>=0.9.1',
        'psycopg2>=2.9.5',
    ],

    extras_require={
        'testing': [
            'pytest>=7.2.1',
            'pytest-flask>=0.15.1',
            'testing.postgresql>=1.3.0',
            'pyyaml>=6.0',
        ],
    },

    # Project urls
    project_urls={
        'Bug Reports': 'https://salsa.debian.org/snapshot-team/snapshot/issues',
        'Source': 'https://salsa.debian.org/snapshot-team/snapshot',
    },
)
