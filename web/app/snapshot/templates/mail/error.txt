Snapshot web application encountered an exception:

HTTP request:

{{ url }}

HTTP response:

{{ exception.code }} {{ exception.name }}
{{ exception.description }}

Original exception:

{{ trace }}

-- 

Snapshot web app
